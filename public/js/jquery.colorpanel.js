(function ($) {

    $.fn.ColorPanel = function (options) {

        var panelDiv = $("#colorPanel .switcher-content");
       
        var settings = $.extend({
            styleSheet: '#cpswitch',
            colors: {
                '#1abc9c': 'skins/default.css',
                '#2980b9': 'skins/blue.css',
                '#c0392b': 'skins/red.css'
            },
            linkClass: 'linka',
            animateContainer: panelDiv
        }, options);
 
        $('#cpToggle').click(function(e){
            e.preventDefault();
            panelDiv.parent().toggleClass('panelAnimation');
        });
        
        var colors = settings.colors || null;

        if (colors) {

            $.each(colors, function (key, value) {
                var li = $("<li/>");
                var e = $("<a />", {
                    href: value
                    , "class": settings.linkClass, // you need to quote "class" since it's a reserved keyword
                }).css('background-color', key);
                li.append(e);
                $(panelDiv).find('ul').append(li);
            });

                    
            if ( readCookie('color') ) {
                    
                var CssFile = $(this).attr('href') || readCookie('color') ;
                createCookie('color', CssFile , 30);
                if (settings.animateContainer) {
                    $(settings.animateContainer).fadeOut(function () {
                        $(settings.styleSheet).attr('href', CssFile);
                        // And then:
                        $(this).fadeIn();
                    });
                }
                else {
                    $(settings.styleSheet).attr('href', CssFile);
                }
               
            }

            $('ul',panelDiv).on('click', 'a', function (e) {
                e.preventDefault();
                var CssFile = $(this).attr('href') || readCookie('color') || 'default.css' ;
                createCookie('color', CssFile , 30);
                if (settings.animateContainer) {
                    $(settings.animateContainer).fadeOut(function () {
                        $(settings.styleSheet).attr('href', CssFile);
                        // And then:
                        $(this).fadeIn();
                    });
                }
                else {
                    $(settings.styleSheet).attr('href', CssFile);
                }
            });
        }

    }



    // functions for cookies
    function createCookie(name,value,days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
    }

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    function eraseCookie(name) {
        createCookie(name,"",-1);
    }

}(jQuery));