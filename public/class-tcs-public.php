<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       orionorigin.com
 * @since      1.0.0
 *
 * @package    Tcs
 * @subpackage Tcs/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Tcs
 * @subpackage Tcs/public
 * @author     Orion <support@orionorigin.com>
 */
class Tcs_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Tcs_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Tcs_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/tcs-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Tcs_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Tcs_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( "jquery-ui", 'http://code.jquery.com/ui/1.10.3/jquery-ui.js', array( 'jquery' ), $this->version, false );

		wp_enqueue_script( "color_switcher_lib", plugin_dir_url( __FILE__ ) . 'js/jquery.colorpanel.js', array( 'jquery' ), $this->version, true );

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/tcs-public.js', array( 'jquery' ), $this->version, true );

		wp_localize_script( $this->plugin_name, 'styles_link', array(
				'base' => get_stylesheet_directory_uri()
			)
		);


	}

	
    public function cana_color_panel_html() {
        ?>
        <div id="colorPanel" class="colorPanel">
            <a id="cpToggle" href="#">
            	<div class="settings-icon">
		    		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" enable-background="new 0 0 64 64">
		    			<g fill="none" stroke="#000" stroke-width="2" stroke-miterlimit="10">
			    			<path d="m32 1h-6v9l-6 2-6-6-8 8 6 6-2 6h-9v12h9l2 6-6 6 8 8 6-6 6 2v9h6 6v-9l6-2 6 6 8-8-6-6 2-6h9v-12h-9l-2-6 6-6-8-8-6 6-6-2v-9z">
			    			</path>
			    			<circle cx="32" cy="32" r="6">
			    			</circle>
		    			</g>
		    		</svg>
		    	</div>
            </a>
            <div class="switcher-content">
                <div class="switcher-colors">
                    <p class="instructions">
                        Lorem Elsass ipsum réchime amet sed bissame so libero. DNA, leo Richard Schirmeck tellus
                    </p>

                    <div class="colors-list">
                        <ul></ul>
                    </div>
                </div>

                <div class="themes-demos">
                 
                </div>
            </div>
        </div>

        <?php
    }

}
