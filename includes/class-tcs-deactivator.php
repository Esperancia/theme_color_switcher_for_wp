<?php

/**
 * Fired during plugin deactivation
 *
 * @link       orionorigin.com
 * @since      1.0.0
 *
 * @package    Tcs
 * @subpackage Tcs/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Tcs
 * @subpackage Tcs/includes
 * @author     Orion <support@orionorigin.com>
 */
class Tcs_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
