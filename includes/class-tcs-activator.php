<?php

/**
 * Fired during plugin activation
 *
 * @link       orionorigin.com
 * @since      1.0.0
 *
 * @package    Tcs
 * @subpackage Tcs/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Tcs
 * @subpackage Tcs/includes
 * @author     Orion <support@orionorigin.com>
 */
class Tcs_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
